import styled from 'styled-components';
import { Layout, Form, Input, Button, Typography } from 'antd';
import { useAuth } from '../context/auth';

const { Content } = Layout;
const { Title } = Typography;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 6, span: 16 },
};

const ContentCenter = styled(Content)`
  height: calc(100vh - 10px);
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const Box = styled.div`
  width: 500px;
  background-color: #ffffff;
  padding: 20px;
`

export default function Login() {
  const auth = useAuth()
  console.log('auth', auth)

  const onFinish = async (values) => {
    const data = await auth.signIn(values)
    console.log('data', data)
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <Layout>
      <ContentCenter>
        <Title level={1} style={{ textAlign: 'center', marginBottom: 50 }}>Media Management</Title>
        <Box>
          <Title level={2} style={{ textAlign: 'center', marginBottom: 20 }}>Login</Title>
          <Form
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="Username"
              name="username"
              rules={[{ required: true, message: 'Please input your username!' }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: 'Please input your password!' }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout} style={{ marginTop: 10 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Box>
      </ContentCenter>
    </Layout>
  )
}