import React from 'react';
import Upload from '../components/Upload'
import Layout from '../components/Layout'


export default function Dashboard() {
  return (
    <Layout page="dashboard">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <div style={{ width: '80%', height: 300 }}>
          <Upload />
        </div>
      </div>
    </Layout>
  )
}