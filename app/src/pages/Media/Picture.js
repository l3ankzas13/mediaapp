import { useState, useEffect } from 'react'
import axios from 'axios'
import MediaCard from '../../components/MediaCard'


import { useAuth } from '../../context/auth'
import Layout from '../../components/Layout'


export default function PictureMedia() {
  const { token } = useAuth()
  const [media, setMedia] = useState([])
  useEffect(() => {
    const fetchMedia = async () => {
      const res = await axios.get('http://157.230.33.40:3001/media/query?fileType=image', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
      })
      if (res.status === 200 && res.data.data) {
        setMedia(res.data.data)
      }
    }
    if (token) fetchMedia()
  }, [token])

  const handleRemove = async (status) => {
    if (status === 'success') {
      const res = await axios.get('http://157.230.33.40:3001/media/query?fileType=image', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      if (res.status === 200 && res.data.data) {
        setMedia(res.data.data)
      }
    }
  }

  return (
    <Layout page="picture" tab="sub1">
      <div style={{ width: '800px' }} >
        <div className="content">
          {media.map((file) => (
            <MediaCard file={file} onRemove={handleRemove} />
          ))}
        </div>
      </div>
    </Layout>
  )
}