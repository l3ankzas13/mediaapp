import { useState, useEffect } from 'react'
import axios from 'axios'
import { Select, Form, Input, Button, DatePicker } from 'antd';
import MediaCard from '../../components/MediaCard';

import { useAuth } from '../../context/auth'
import Layout from '../../components/Layout'

const { Option } = Select;


export default function AllMedia() {
  const { token } = useAuth()
  const [media, setMedia] = useState([])
  const [searchBy, setSearchBy] = useState('fileType')
  const [searchValue, setSearchValue] = useState('all')

  useEffect(() => {
    const fetchMedia = async () => {
      const res = await axios.get('http://157.230.33.40:3001/media', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      if (res.status === 200 && res.data.data) {
        setMedia(res.data.data)
      }
    }
    if (token) fetchMedia()
  }, [token])

  const fetchAllData = async () => {
    const res = await axios.get('http://157.230.33.40:3001/media', {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    if (res.status === 200 && res.data.data) {
      setMedia(res.data.data)
    }
  }

  const handleRemove = async (status) => {
    if (status === 'success') {
      fetchAllData()
    }
  }

  const handleSearch = async () => {
    const res = await axios.get(`http://157.230.33.40:3001/media/query?${searchBy}=${searchValue}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })
    if (res.status === 200 && res.data.data) {
      setMedia(res.data.data)
    }
  }

  const handleClear = async () => {
    setSearchBy('fileType')
    setSearchValue('all')
    fetchAllData()
  }

  return (
    <Layout page="all" tab="sub1">
      <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
        <Form.Item label="Search By">
          <Select defaultValue="fileType" value={searchBy} style={{ width: 120 }} onChange={(value) => setSearchBy(value)}>
            <Option value="fileType">File Type</Option>
            <Option value="name">File Name</Option>
            <Option value="creator">Creator</Option>
            <Option value="createdDate">Created Date</Option>
          </Select>
        </Form.Item>
        <Form.Item style={{ marginLeft: 10 }}>
          {searchBy === 'createdDate' && <DatePicker onChange={(value) => setSearchValue(value)} />}
          {['name', 'creator'].includes(searchBy) && (<Input onChange={(e) => setSearchValue(e.target.value)} />)}
          {searchBy === 'fileType' && (
            <Select defaultValue="all" style={{ width: 120 }} onChange={(value) => setSearchValue(value)}>
              <Option value="all">All</Option>
              <Option value="image/png">image/png</Option>
              <Option value="image/jpg">image/jpg</Option>
              <Option value="image/jpeg">image/jpeg</Option>
              <Option value="video/mp4">video/mp4</Option>
            </Select>
          )}
        </Form.Item>
        <Button type="primary" onClick={handleSearch} style={{ marginLeft: 10 }}>Search</Button>
        <Button type="primary" onClick={handleClear} style={{ marginLeft: 10 }}>Clear</Button>
      </div>
      <div style={{ width: '800px' }} >
        <div className="content">
          {media.map((file) => (
            <MediaCard file={file} onRemove={handleRemove} />
          ))}
        </div>
      </div>
    </Layout>
  )
}