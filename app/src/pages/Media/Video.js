import { useState, useEffect } from 'react'
import axios from 'axios'
import { Card } from 'antd';
import MediaCard from '../../components/MediaCard';


import { useAuth } from '../../context/auth'
import Layout from '../../components/Layout'

export default function AllMedia() {
  const { token } = useAuth()
  console.log('token', token)
  const [media, setMedia] = useState([])
  useEffect(() => {
    const fetchMedia = async () => {
      const res = await axios.get('http://157.230.33.40:3001/media/query?fileType=video', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      if (res.status === 200 && res.data.data) {
        setMedia(res.data.data)
      }
    }
    if (token) fetchMedia()
  }, [token])

  const handleRemove = async (status) => {
    if (status === 'success') {
      const res = await axios.get('http://157.230.33.40:3001/media/query?fileType=video', {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
      if (res.status === 200 && res.data.data) {
        setMedia(res.data.data)
      }
    }
  }

  return (
    <Layout page="video" tab="sub1">
      <div style={{ width: '800px' }} >
        <div className="content">
          {media.map((file) => (
            <MediaCard file={file} onRemove={handleRemove} />
          ))}
        </div>
      </div>
    </Layout>
  )
}