import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import AllMedia from './pages/Media/All';
import PictureMedia from './pages/Media/Picture';
import VideoMedia from './pages/Media/Video';

import { ProvideAuth, PrivateRoute } from './context/auth';

import './App.css';

function App() {
  return (
    <Router>
      <ProvideAuth>
        <Switch>
          <PrivateRoute path="/" exact>
            <Redirect to="/dashboard" />
          </PrivateRoute>
          <Route path="/login">
            <Login />
          </Route>
          <PrivateRoute path="/dashboard">
            <Dashboard />
          </PrivateRoute>
          <PrivateRoute path="/media/all">
            <AllMedia />
          </PrivateRoute>
          <PrivateRoute path="/media/picture">
            <PictureMedia />
          </PrivateRoute>
          <PrivateRoute path="/media/video">
            <VideoMedia />
          </PrivateRoute>
        </Switch>
      </ProvideAuth>
    </Router>
  );
}

export default App;
