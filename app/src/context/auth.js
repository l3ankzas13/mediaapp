import React, { useContext, createContext, useState, useEffect } from "react";
import {
  Route,
  Redirect,
  useHistory
} from "react-router-dom";
import axios from 'axios';

export const authContext = createContext();

export function ProvideAuth({ children }) {
  const auth = useProvideAuth();
  return (
    <authContext.Provider value={auth}>
      {children}
    </authContext.Provider>
  );
}

export function useAuth() {
  return useContext(authContext)
}

export function getLocalItem(key) {
  try {
    return JSON.parse(localStorage.getItem(key))
  } catch (error) {
    return ''
  }
}

export function useProvideAuth() {
  const [user, setUser] = useState(getLocalItem('profile'));
  const [token, setToken] = useState(localStorage.getItem('token'));
  let history = useHistory();

  // useEffect(() => {
  //   console.log('token)', getLocalItem('token'))
  //   setUser(getLocalItem('profile'))
  //   setToken(getLocalItem('token'))
  // }, [])

  const getProfile = async (token) => {
    try {
      const userResponse = await axios.get('http://157.230.33.40:3001/user/profile', { headers: { 'Authorization': `Bearer ${token}` } })
      if (userResponse.data.data) {
        return userResponse.data.data
      }
    } catch (error) {
      return;
    }
  }

  const signIn = async ({ username, password }) => {
    try {
      const res = await axios.post('http://157.230.33.40:3001/login', {
        username,
        password
      })

      if (res.data && res.data.token) {
        localStorage.setItem('token', res.data.token);
        setToken(res.data.token)
        const profile = await getProfile(res.data.token)
        if (profile) {
          setUser(profile)
          localStorage.setItem('profile', JSON.stringify(profile))
          history.push('/dashboard')
        }
      }
    } catch (error) {
      console.log('error', error)
    }
  }


  const signOut = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('profile');
    setUser(null);
  };


  return {
    user,
    token,
    signIn,
    signOut
  };
}


export function PrivateRoute({ children, ...rest }) {
  let auth = useAuth();

  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.user ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}

