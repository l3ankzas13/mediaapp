import React from 'react';
import { Layout, Menu, Typography, Button } from 'antd';
import { useAuth } from '../../context/auth';
import { useHistory } from 'react-router-dom';

const { Title } = Typography
const { Header, Content, Sider } = Layout;
const { SubMenu } = Menu;

export default function LayoutComponent({ children, page, tab }) {
  const auth = useAuth()
  let history = useHistory()

  const handleChangePage = (page) => () => {
    history.push(page)
  }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header className="header" style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <Title level={3} style={{ color: 'white', margin: 0 }}>Media Management System</Title>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
          <Title level={5} style={{ color: 'white', margin: 0, marginRight: 10 }}>สวัสดีคุณ {auth.user.firstName}</Title>
          <Button type="danger" onClick={auth.signOut}>ออกจากระบบ</Button>
        </div>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu
            mode="inline"
            defaultSelectedKeys={[page]}
            defaultOpenKeys={tab ? [tab] : []}
            style={{ height: '100%', borderRight: 0 }}
          >
            <Menu.Item key="dashboard" onClick={handleChangePage('/dashboard')}>Upload</Menu.Item>
            <SubMenu key="sub1" title="Media">
              <Menu.Item key="all" onClick={handleChangePage('/media/all')}>All</Menu.Item>
              <Menu.Item key="picture" onClick={handleChangePage('/media/picture')}>Picture</Menu.Item>
              <Menu.Item key="video" onClick={handleChangePage('/media/video')}>Video</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout style={{ padding: '0 24px 24px', maxWidth: '100%' }}>
          <Content
            className="site-layout-background"
            style={{
              padding: 24,
              margin: 0,
              minHeight: 280,
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  )
}