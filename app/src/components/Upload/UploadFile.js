
import { Upload } from 'antd';
import { InboxOutlined } from '@ant-design/icons';

const { Dragger } = Upload;


export default function UploadFile({ fileUrl, fileType, ...props }) {
  if (fileUrl && fileType) {
    if (fileType === 'video/mp4') {
      return (
        <video width="100%" height="400" controls >
          <source src={fileUrl} type="video/mp4" />
        </video>
      )
    }
    return (<img src={fileUrl} alt="" width="100%" height="400" />)
  }
  return (
    <Dragger
      {...props}
    >
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">Click or drag file to this area to upload</p>
      <p className="ant-upload-hint">
        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
        band files
  </p>
    </Dragger>
  )
}