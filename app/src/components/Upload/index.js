import { useState } from 'react'
import { message, Form, Input, Button } from 'antd';
import axios from 'axios'

import { useAuth } from '../../context/auth';
import UploadFile from './UploadFile';


function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

export default function FileUpload() {
  const [form] = Form.useForm();
  const [preview, setPreview] = useState({ fileUrl: '', fileType: '' })
  const [fileList, setFileList] = useState([])
  const [loading, setLoading] = useState(false)
  const auth = useAuth();

  const props = {
    onRemove: file => {
      setFileList(list => {
        const index = list.indexOf(file);
        const newFileList = list.fileList.slice();
        newFileList.splice(index, 1);
        return newFileList
      });
    },
    beforeUpload: file => {
      const isValidFile = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'video/mp4';
      if (!isValidFile) {
        message.error('You can only upload JPG/PNG/MP4 file!');
      }
      const isLt50M = file.size / 1024 / 1024 < 50;
      if (!isLt50M) {
        message.error('Image must smaller than 50MB!');
      }
      if (isValidFile && isLt50M) setFileList(list => ([...list, file]));
      return isValidFile && isLt50M
    },
    onChange: ({ fileList }) => {
      const isValidFile = fileList[0].type === 'image/jpeg' || fileList[0].type === 'image/png' || fileList[0].type === 'video/mp4';
      const isLt50M = fileList[0].size / 1024 / 1024 < 50;
      if (isValidFile && isLt50M) {
        setFileList(fileList)
        getBase64(fileList[0].originFileObj, fileUrl =>
          setPreview({
            fileUrl,
            fileType: fileList[0].type
          }),
        );
      }
    },
    fileList,
  };

  const layout = {
    labelCol: { span: 3 },
    wrapperCol: { span: 10 },
  };
  const tailLayout = {
    wrapperCol: { offset: 3, span: 16 },
  };

  const handleFinish = async (values) => {
    setLoading(true)
    try {
      console.log('fileList', fileList)
      if (fileList.length === 0) {
        message.error('Please input file')
        return;
      }
      let formData = new window.FormData();
      formData.append('file', fileList[0].originFileObj);
      formData.append('name', values.name);
      const data = await axios.post('http://157.230.33.40:3001/media/upload',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data', Authorization: `Bearer ${auth.token}`
          }
        }
      )
      if (data.status === 200) {
        message.success('Upload success')
        form.resetFields()
        handleReset()
      } else {
        message.error('Upload fail try again')
      }
      return;
    } catch (error) {
      console.log('error', error)
      form.resetFields()
    } finally {
      setLoading(false)
    }
  }

  const handleReset = () => {
    setFileList([])
    setPreview({})
  }

  return (
    <Form {...layout} form={form} name="control-hooks" onFinish={handleFinish}>
      <UploadFile {...props} {...preview} />
      <Form.Item name="name" label="File name" rules={[{ required: true }]} style={{ marginTop: 20 }}>
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout} >
        <Button type="primary" htmlType="submit" loading={loading}>
          Submit
        </Button>
        <Button type="danger" onClick={handleReset} style={{ marginLeft: 10 }} loading={loading}>
          Cancel
        </Button>
      </Form.Item>
    </Form>
  )
}