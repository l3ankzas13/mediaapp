import { useState } from 'react';
import { Card, Tag, Button, message } from 'antd';
import axios from 'axios';
import dayjs from 'dayjs';

import { useAuth } from '../../context/auth';

const { Meta } = Card;

const RenderContent = ({ fileType, filePath }) => {
  if (fileType) {
    if (fileType === 'video/mp4') {
      return (
        <video width="100%" height="300" controls >
          <source src={`http://157.230.33.40:3001/view${filePath}`} type="video/mp4" />
        </video>
      )
    }
    return (<img src={`http://157.230.33.40:3001/view${filePath}`} alt="" width="100%" height="300" />)
  }
}

export default function MediaCard({ file, onRemove = () => { } }) {
  const { token } = useAuth()
  const [loading, setLoading] = useState(false)

  const handleRemove = (id) => async () => {
    setLoading(true)
    try {
      const res = await axios.delete('http://157.230.33.40:3001/media',
        {
          params: { id },
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
          }
        }
      )
      if (res.status === 200) {
        message.success('Remove success')
        onRemove('success')
      } else {
        message.success('Remove fail')
        onRemove('fail')
      }
    } catch (error) {
      onRemove('fail')
    } finally {
      setLoading(false)
    }
  }

  return (
    <Card
      key={file.id}
      hoverable
      style={{ width: 350 }}
      cover={<RenderContent {...file} />}
    >
      <Meta title={`Name: ${file.name}`} description={`Creator: ${file.user.firstName} ${file.user.lastName}`} />
      <Tag color="magenta" style={{ marginTop: 10 }}>{file.fileType}</Tag>
      <Tag color="magenta" style={{ marginTop: 10 }}>{`Create At: ${dayjs(file.createdAt).format('DD/MM/YY HH:mm:ss')}`}</Tag>
      <div style={{ width: '100%', display: 'flex', justifyContent: 'space-around', paddingTop: 30 }}>
        <Button loading={loading} onClick={() => message.warn('Sorry this feature comming soon :(')}>Update</Button>
        <Button loading={loading} htmlType="a" type="primary" target="_blank" href={`http://157.230.33.40:3001/view${file.filePath}`} download>Download</Button>
        <Button loading={loading} type="danger" ghost onClick={handleRemove(file.id)}>Remove</Button>
      </div>
    </Card>
  )
}