# Media App

### Clone Project
```
git clone https://gitlab.com/l3ankzas13/mediaapp.git mediaapp
cd mediaapp
```

### File Stucture
```
  - folder app is react app
  - folder server is node service API
  - folder nginx is reverse proxy
```

### Start with Docker
```
docker-compose up -d --build
```

### Migrate Database
```
docker exec -it media_cms_api npx sequelize-cli db:migrate
```

### Add mockup data
```
docker exec -it media_cms_api npx sequelize-cli db:seed:all
```

### Log monitor
```
docker logs codemonday_app --follow
```

### React App run on part 3000
```
http://localhost:3000

username and password for get started
- user: admin
- password: admin123456
```

### Node Service API run on port 3001
```
http://localhost:3001
```

### Environments (I set up environment in file docker-compose.yml)
```
Database using postgresql
 - User: admin
 - Password: 413ego8ebjx54wxi
 - DB Name: media-db

You can ser database structure in folder /server/database/migrations

```

### API Documentation
```
// todo update later
http://localhost:3000/api-docs
```

### What to do after this
```
- Refactor code front-end and back-end
- Improve Responsive for ui
- Add react-window for render list to be effective
- Add api document
```


