const bcrypt = require('bcrypt');

async function generatePassword(password) {
  const saltRounds = 10;
  const salt = await bcrypt.genSalt(saltRounds);
  const hasPass = await bcrypt.hash(password, salt)
  console.log('hasPass', hasPass);
  return hasPass;
}

async function validatePassword(password, hash) {
  const valid = await bcrypt.compare(password, hash);
  console.log('valid', valid);
  return valid;
}

module.exports = {
  generatePassword,
  validatePassword
}
