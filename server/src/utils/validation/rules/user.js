const findTeacherValidationRules = {
  id: {
    required: true,
    error: 'please send id'
  },
}

const createUserValidationRules = {
  firstName: {
    required: true,
    error: 'Please input First Name'
  },
  lastName: {
    required: true,
    error: 'Please input Last Name'
  },
  email: {
    required: true,
    error: 'Please input email'
  },
  username: {
    required: true,
    error: 'Please input username'
  },
  password: {
    required: true,
    error: 'Please input password'
  }
}

module.exports = { createUserValidationRules }