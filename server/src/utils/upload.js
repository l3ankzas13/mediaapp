const multer = require('multer');

const TYPE_IMAGE = {
  'image/png': 'png',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg',
  'video/mp4': 'mp4',
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/api/public/uploads')
  },
  filename: function (req, file, cb) {
    const ext = (TYPE_IMAGE[file.mimetype]) ? TYPE_IMAGE[file.mimetype] : TYPE_File[file.mimetype];
    cb(null, `${file.fieldname}-${Date.now()}.${ext}`);
  },
  fileFilter: (req, file, cb) => {
    let size = +req.rawHeaders.slice(-1)[0]
    let isValid = false;
    if (!!TYPE_IMAGE[file.mimetype] && size < 50 * 1024 * 1024) {
      isValid = true
    }
    let error = isValid ? null : new Error('Invalid mime type!');
    cb(error, isValid);
  }
})

const upload = multer({ storage })

module.exports = upload;