const passport = require('passport');
const models = require('../../database/models')
const { validatePassword } = require('../password')

const secretOrKey = 'd39MOxjhVUyqabA9WARs0wZoBIvleRh5fzfMCMk0CwfBAV2M0v'

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const localStrategy = require('passport-local').Strategy;
let opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey,
}

passport.use(
  'login',
  new localStrategy(
    {
      usernameField: 'username',
      passwordField: 'password'
    },
    async (username, password, done) => {
      try {
        const user = await models.User.findOne({ username });
        if (!user) {
          return done(null, false, { message: 'User not found' });
        }

        const validate = await validatePassword(password, user.password);

        if (!validate) {
          return done(null, false, { message: 'Wrong Password' });
        }
        return done(null, user, { message: 'Logged in Successfully' });
      } catch (error) {
        return done(error);
      }
    }
  )
);


passport.use('jwt', new JwtStrategy(opts, async (jwt_payload, done) => {
  try {
    const user = await models.User.findOne({
      id: jwt_payload._id,
      attributes: { exclude: ['password', 'createdAt', 'updatedAt'] }
    })
    if (user) return done(null, user);
    return done(null, false);
  } catch (error) {
    console.log('error', error)
    return done(err, false);
  }
}));