const express = require('express');
const router = express.Router();
const models = require('../database/models')
const validator = require('../utils/validation/validator')
const validateRules = require('../utils/validation/rules/user')
const jwt = require('jsonwebtoken');
const passport = require('passport');


router.get('/profile', async (req, res, next) => {
  const user = req.user
  return res.status(200).json({ message: 'success', errors: '', data: user })
});

router.get('/:id', async (req, res, next) => {
  const { id = '' } = req.params;
  const user = await models.User.findOne({ id })
  res.status(200).json({ data: user })
});

router.post('/', async (req, res, next) => {
  let message = 'Created success';
  let status = 400;
  let errors = '';
  try {
    const validated = validator(req?.body, validateRules.createUserValidationRules)
    if (!validated.isValid) {
      message = 'error';
      status = 400;
      errors = validated.errors;
      return;
    }
    const user = await models.User.create(req?.body)
    if (user) {
      message = 'Created success';
      status = 200;
      return;
    }
    message = 'Created fail';
    status = 400;
    errors = 'something went wrong';
    return;
  } catch (error) {
    console.log('error', error)
    message = 'error';
    status = 400;
    errors = 'something went wrong';
    return;
  } finally {
    res.status(status).json({ message, errors })
  }
});

module.exports = router;
