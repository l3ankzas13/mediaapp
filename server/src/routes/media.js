const express = require('express');
const { Op } = require("sequelize");
const dayjs = require("dayjs");

const upload = require('../utils/upload');
const models = require('../database/models');

const router = express.Router();

// todo: refactor response
router.post('/upload', upload.single('file'), async (req, res, next) => {
  const file = req.file
  const user = req.user
  console.log('user', user)
  if (!file) {
    res.status(400).json({ error: 'Please upload a file' })
    next();
  }
  const data = await models.Media.create({
    name: req.body.name,
    fileName: file.filename,
    filePath: `/${file.filename}`,
    fileFullPath: file.path,
    fileType: file.mimetype,
    userId: user.id,
  })
  if (data) {
    return res.status(200).json({ message: 'Upload success', errors: '' })
  }
  return res.status(400).json({ message: 'Upload fail', errors: 'something went wrong' })
});

router.get('/', async (req, res) => {
  try {
    const data = await models.Media.findAll({
      include: [{
        model: models.User,
        as: 'user',
        attributes: ['id', 'firstName', 'lastName', 'username']
      }],
      order: [['id', 'DESC']]
    })
    return res.status(200).json({ data })
  } catch (error) {
    res.status(400).json({ message: 'Failed', errors: 'something went wrong' })
  }
})

router.get('/query', async (req, res) => {
  const { id = '', name = '', fileType = '', creator = '', createdDate } = req.query
  let where = {}
  let userWhere = {}

  if (id) where.id = id
  if (name) where.name = { [Op.like]: `%${name}%` }
  if (fileType && fileType !== 'all') where.fileType = { [Op.like]: `%${fileType}%` }
  if (creator) {
    const [fName = '', lName = ''] = creator.split(' ')
    userWhere.firstName = { [Op.like]: `%${fName}%` }
    if (lName) {
      userWhere.lastName = { [Op.like]: `%${lName}%` }
    }
  }
  if (createdDate) {
    where.createdAt = {
      [Op.gte]: dayjs(createdDate).startOf('day').format('YYYY-MM-DD HH:mm:ss'),
      [Op.lte]: dayjs(createdDate).endOf('day').format('YYYY-MM-DD HH:mm:ss'),
    }
  }
  if (!id && !name && !fileType && !creator && !createdDate) {
    return res.status(400).json({ message: 'Failed', errors: 'something went wrong' })
  }

  try {
    const data = await models.Media.findAll({
      include: [{
        model: models.User,
        as: 'user',
        attributes: ['id', 'firstName', 'lastName', 'username'],
        where: userWhere
      }],
      where,
      order: [['id', 'DESC']]
    })
    return res.status(200).json({ data })
  } catch (error) {
    res.status(400).json({ message: 'Failed', errors: 'something went wrong' })
  }
})

router.delete('/', async (req, res) => {
  const { id } = req.query
  try {
    if (!id) return res.status(400).json({ message: 'Remove fail', errors: 'something went wrong' })
    const data = await models.Media.destroy({ where: { id } })
    return res.status(200).json({ data, message: 'Remove Success' })
  } catch (error) {
    res.status(400).json({ message: 'Remove Fail', errors: 'something went wrong' })
  }
})


module.exports = router;
