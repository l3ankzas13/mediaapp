const express = require('express');
const router = express.Router();
const models = require('../database/models')
const validator = require('../utils/validation/validator')
const validateRules = require('../utils/validation/rules/user')
const jwt = require('jsonwebtoken');
const passport = require('passport');

router.post(
  '/',
  async (req, res, next) => {
    passport.authenticate(
      'login',
      async (err, user, info) => {
        let message = 'success';
        let status = 200;
        let errors = '';
        let token = '';
        try {
          if (err || !user) {
            message = 'error';
            status = 400;
            errors = 'something went wrong';
            return;
          }

          req.login(
            user,
            { session: false },
            async (error) => {
              if (error) {
                message = 'error';
                status = 400;
                errors = 'something went wrong';
                return;
              };

              const body = { _id: user.id, username: user.username };
              token = jwt.sign({ user: body }, 'd39MOxjhVUyqabA9WARs0wZoBIvleRh5fzfMCMk0CwfBAV2M0v');
              return;
            }
          );
        } catch (error) {
          message = 'error';
          status = 400;
          errors = 'something went wrong';
          return;
        } finally {
          return res.status(status).json({ token, message, errors });
        }
      }
    )(req, res, next);
  }
);


module.exports = router;
