const express = require('express');
const logger = require('morgan');
const passport = require('passport');
const cors = require('cors')

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const mediaRouter = require('./routes/media');
const loginRouter = require('./routes/login');

const app = express();

app.use(logger('dev'));
app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());
require('./utils/auth');

app.use('/', indexRouter);
app.use('/user', passport.authenticate('jwt', { session: false }), usersRouter);
app.use('/media', passport.authenticate('jwt', { session: false }), mediaRouter);
app.use('/login', loginRouter);
app.get('/view/:name', (req, res) => {
  const { name = '' } = req.params
  const options = {
    root: '/api/public/uploads/'
  };
  const fileName = `${name}`;
  res.sendFile(fileName, options);
})



app.listen(process.env.PORT, () => {
  console.log('server listening on port: ', process.env.PORT)
})

module.exports = app;
