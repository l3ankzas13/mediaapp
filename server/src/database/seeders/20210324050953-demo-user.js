'use strict';

const { generatePassword } = require('../../utils/password')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      username: 'admin',
      password: await generatePassword('admin123456'),
      firstName: 'Admin',
      lastName: 'Media',
      email: 'aekkawan.k@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
