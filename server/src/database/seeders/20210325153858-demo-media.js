'use strict';


const allFile = [
  'file-1616686492560.jpeg',
  'file-1616686501245.jpeg',
  'file-1616686515512.jpeg',
  'file-1616686541328.jpeg',
  'file-1616686541328.jpeg',
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Media', Array.from({ length: 1200 }, (v, i) => {
      return {
        name: `${Math.random().toString(36).substring(2)} ${i + 1}`,
        filePath: `/${allFile[Math.floor(Math.random() * 4) + 1]}`,
        fileFullPath: `/api/public/uploads/${allFile[Math.floor(Math.random() * 4) + 1]}`,
        fileType: 'image/jpeg',
        fileName: allFile[Math.floor(Math.random() * 4) + 1],
        userId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    }));
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Media', null, {});
  }
};
