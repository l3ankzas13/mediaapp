'use strict';

module.exports = (sequelize, DataTypes) => {
  const Media = sequelize.define('Media', {
    name: DataTypes.STRING,
    filePath: DataTypes.STRING,
    fileFullPath: DataTypes.STRING,
    fileType: DataTypes.STRING,
    fileName: DataTypes.STRING,
    userId: DataTypes.INTEGER,
  })


  Media.associate = models => {
    Media.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'userId',
      targetKey: 'id',
    });
  };

  return Media;
};
