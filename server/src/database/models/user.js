'use strict';

const { generatePassword } = require('../../utils/password');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING
  })

  User.associate = models => {
    User.hasMany(models.Media, {
      as: 'user',
      foreignKey: 'userId',
      targetKey: 'id',
    });
  };


  User.beforeCreate(async (user, options) => {
    const hash = await generatePassword(user.password)
    return user.password = hash
  });

  return User;
};
